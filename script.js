(function () {
  const form = document.getElementById("js-form-calculation");
  const input = document.getElementById("js-input-field");
  const result = document.getElementById("js-result-field");
  const byn = document.getElementById("js-input-byn");
  const pln = document.getElementById("js-input-pln");
  const usd = document.getElementById("js-input-usd");
  const date = document.getElementById("js-input-date");
  const inputVal = document.getElementById("js-input-value");
  const resultVal = document.getElementById("js-result-value");
  let price;

  function httpGet(theUrl) {
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false);
    xmlHttp.send(null);
    return xmlHttp.responseText;
  }
  const currencies = JSON.parse(
    httpGet("https://sneaker-head.by/api/currency/rates")
  );

  function eurToBynGet(theUrl) {
    let xp = new XMLHttpRequest();
    xp.open("GET", theUrl, false);
    xp.send(null);
    return xp.responseText;
  }

  const eurToByn = JSON.parse(
    eurToBynGet("https://sneaker-head.by/api/currency/from-pln?price=100")
  );

  const rate = currencies.BYN / currencies.USD;
  console.log(currencies);
  console.log(eurToByn);

  window.onload = function () {
    input.value = eurToByn.price;
    result.innerHTML = Math.floor(eurToByn.converted * 100) / 100;
    if (!inputVal.value) {
      inputVal.value = 1;
      resultVal.innerHTML =
        Math.floor(((inputVal.value * currencies.BYN) / currencies.PLN) * 100) /
        100;
    }
  };

  function renderResult() {
    byn.innerHTML = Math.floor((currencies.BYN / currencies.PLN) * 100) / 100;
    pln.innerHTML = Math.floor((currencies.PLN / currencies.BYN) * 100) / 100;
    // usd.innerHTML = Math.floor(currencies.USD * 100) / 100;
    date.innerHTML = currencies.date;
  }
  renderResult();

  inputVal.addEventListener("keyup", function (event) {
    // console.log(event.key);
    console.log(inputVal.value.indexOf("0"));

    if (
      (event.key == 0 ||
        event.key == 1 ||
        event.key == 2 ||
        event.key == 3 ||
        event.key == 4 ||
        event.key == 5 ||
        event.key == 6 ||
        event.key == 7 ||
        event.key == 8 ||
        event.key == 9 ||
        event.key == ".") &&
      event.key !== " "
    ) {
      resultVal.innerHTML =
        Math.floor(((inputVal.value * currencies.BYN) / currencies.PLN) * 100) /
        100;
    } else if (event.key == " ") {
      inputVal.value = "";
      resultVal.innerHTML = 0;
    } else {
      inputVal.value = "";
      resultVal.innerHTML = 0;
    }
  });

  //
  //
  //
  input.addEventListener("keyup", function (event) {
    if (
      (event.key == 0 ||
        event.key == 1 ||
        event.key == 2 ||
        event.key == 3 ||
        event.key == 4 ||
        event.key == 5 ||
        event.key == 6 ||
        event.key == 7 ||
        event.key == 8 ||
        event.key == 9 ||
        event.key == ".") &&
      event.key !== " "
    ) {
      price = parseInt(input.value);
      let url = `https://sneaker-head.by/api/currency/from-pln?price=${price}`;

      let response = JSON.parse(eurToBynGet(url));
      let convertedPrice = response.hasOwnProperty("converted")
        ? response.converted
        : 0;

      result.innerHTML = convertedPrice;
    } else if (input.value.indexOf("0") == 0) {
      price = 0;
    } else if (event.key == " ") {
      input.value = "";
      result.innerHTML = 0;
    } else {
      input.value = "";
      result.innerHTML = 0;
    }
  });
})();
